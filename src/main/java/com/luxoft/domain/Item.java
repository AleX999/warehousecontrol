package com.luxoft.domain;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "ITEMS")
public class Item implements Serializable {

    @Column(name = "COMMENT")
    String comment = "";
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "USER_ID", referencedColumnName = "ID", nullable = false)
    private User user;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "STOREHOUSE_ID", referencedColumnName = "ID", nullable = false)
    private Storehouse storehouse;
    @Column(name = "ARRIVAL_DATE", nullable = false)
    private Date arrivalDate = new Date();
    @Column(name = "STORE_UNTIL", nullable = false)
    private Date storeUntil;
    @Column(name = "STORING")
    private Boolean storing = true;
    @Column(name = "CELLS_TAKEN", nullable = false)
    private Long cellsTaken;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public Date getStoreUntil() {
        return storeUntil;
    }

    public void setStoreUntil(Date storeUntil) {
        this.storeUntil = storeUntil;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Storehouse getStorehouse() {
        return storehouse;
    }

    public void setStorehouse(Storehouse storehouse) {
        this.storehouse = storehouse;
    }

    public Long getCellsTaken() {
        return cellsTaken;
    }

    public void setCellsTaken(Long cellsTaken) {
        this.cellsTaken = cellsTaken;
    }


    public Boolean getStoring() {
        return storing;
    }

    public void setStoring(Boolean storing) {
        this.storing = storing;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }


}
