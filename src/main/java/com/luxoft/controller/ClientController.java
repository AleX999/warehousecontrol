package com.luxoft.controller;

import com.luxoft.domain.User;
import com.luxoft.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClientController {


    @Autowired
    private UserService userService;

    @RequestMapping(value = "/client")
    public String client(ModelMap model) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);
        model.put("listItems", userService.getUserItems(user));

        return "client";
    }
}
