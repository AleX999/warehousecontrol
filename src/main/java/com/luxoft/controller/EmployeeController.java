package com.luxoft.controller;

import com.luxoft.domain.Item;
import com.luxoft.domain.User;
import com.luxoft.exceptions.CantTakeLessThanOneException;
import com.luxoft.exceptions.HaveNoMoreCapacityException;
import com.luxoft.exceptions.WrongStoreDateException;
import com.luxoft.service.ItemService;
import com.luxoft.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.Date;

@Controller
public class EmployeeController {

    @Autowired
    private UserService userService;
    @Autowired
    private ItemService itemService;
    private User client;

    @RequestMapping(value = "/employee", method = RequestMethod.GET)
    public String employee(ModelMap model) {
        client = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);

        model.put("listClients", userService.getUsersList());

        return "employee";
    }

    @RequestMapping(value = "/employee", method = RequestMethod.POST)
    public String back(ModelMap model) {
        client = null;
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);

        model.put("listClients", userService.getUsersList());

        return "employee";
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public String search(@RequestParam("search") String search, ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);
        try {
            client = userService.getUserByEmail(search);
            model.put("client", client);
            model.put("listItems", itemService.getAllStoredItemsByUser(client));
        } catch (Exception e) {
            client = null;
            model.put("listClients", userService.getUsersList());
            model.put("ClientNotFound", "Client not found!");
            return "employee";
        }

        return "items";
    }


    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String getEmailByList(@RequestParam("email") String search, ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);
        try {
            client = userService.getUserByEmail(search);

            model.put("client", client);
            model.put("listItems", userService.getUserStoringItems(client));
        } catch (Exception e) {
            client = null;

            model.put("listClients", userService.getUserStoringItems(client));
            model.put("ClientNotFound", "Client not found!");
            return "employee";
        }


        return "items";
    }


    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addItem(@ModelAttribute("add") Item item, @RequestParam("date") String date, ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);

        if (client == null) {
            model.put("clientNotSelected", "Client not selected");
        } else {
            try {
                model.put("client", client);
                itemService.changeToStoreDate(item, Date.valueOf(date));
                item.setStoreUntil(Date.valueOf(date));
                itemService.addItem(item, client);
                model.put("itemAdd", "Item added successful");


            } catch (WrongStoreDateException e) {
                model.put("dateError", "Store until can't be before today!");
            } catch (HaveNoMoreCapacityException e) {
                model.put("capacityError", "Storehouse has no more capacity");
            } catch (CantTakeLessThanOneException e) {
                model.put("capacityError", "Can't take less than 1!");
            } catch (IllegalArgumentException e) {
                model.put("dateError", "Wrong data!");
            } catch (NullPointerException e) {
                model.put("capacityError", "Please, enter some capacity!");
            }
        }
        model.put("listItems", itemService.getAllStoredItemsByUser(client));

        return "items";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public String deleteItem(@RequestParam("release") Long id, ModelMap model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName();

        User user = userService.getUserByEmail(email);
        model.put("user", user);
        if (client == null) {
            model.put("clientNotSelected", "Client not selected");
        } else {
            try {
                model.put("client", client);
                itemService.unstoreItem(id);
                model.put("client", client);
                model.put("itemDelete", "Item deleted successful");
            } catch (Exception e) {
                model.put("searchItem", "Item not found");
            }
        }
        model.put("listItems", itemService.getAllStoredItemsByUser(client));

        return "items";
    }

}
