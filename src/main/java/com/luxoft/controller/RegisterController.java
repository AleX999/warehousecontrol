package com.luxoft.controller;

import com.luxoft.domain.User;
import com.luxoft.service.RoleService;
import com.luxoft.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class RegisterController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String register() {
        return "register";
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerInDB(@ModelAttribute("register") User user, ModelMap model) {
        try {
            userService.createUser(user);
            roleService.setUserRoleToUser(user);
        } catch (DataIntegrityViolationException e) {
            model.put("email", "User already exists");
            return "register";
        }
        return "index";
    }
}
