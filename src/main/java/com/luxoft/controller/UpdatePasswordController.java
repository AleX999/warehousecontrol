package com.luxoft.controller;


import com.luxoft.domain.User;
import com.luxoft.service.RoleService;
import com.luxoft.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

@Controller
public class UpdatePasswordController {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @RequestMapping(value = "/updateinfo")
    public String client(ModelMap model) {
        return "updateinfo";
    }

    @RequestMapping(value = "/goback")
    public String redirect() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities = (List<GrantedAuthority>) auth.getAuthorities();
        String role = authorities.get(0).toString();

        if (role.equals("ROLE_USER")) {
            return "redirect:/client";
        } else {
            return "redirect:/employee";
        }
    }

    @RequestMapping(value = "/updatepasswd", method = RequestMethod.POST)
    public String updateInDB(@ModelAttribute("updatepasswd") User newUser) {

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        String activeUser;

        if (principal instanceof UserDetails) {
            activeUser = ((UserDetails) principal).getUsername();
        } else {
            activeUser = principal.toString();
        }

        User currentUser = userService.getUserByEmail(activeUser);
        currentUser.setPassword(newUser.getPassword());

        userService.updateUser(currentUser);
        //roleService.setUserRoleToUser(currentUser);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities = (List<GrantedAuthority>) auth.getAuthorities();
        String role = authorities.get(0).toString();

       /* if (role.equals("ROLE_USER")) {
            return "redirect:/client";
        } else {
            return "redirect:/employee";
        }*/

        return "redirect:/logout";
    }

}
