
package com.luxoft.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class LoginController {

    @RequestMapping(value = "/redirect")
    public String redirect() {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String name = auth.getName();

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities = (List<GrantedAuthority>) auth.getAuthorities();
        String role = authorities.get(0).toString();

        if (role.equals("ROLE_USER"))
            return "redirect:/client";
        else return "redirect:/employee";
    }

    @RequestMapping(value = "/failedlogin")
    public String failure(ModelMap modelMap) {
        modelMap.addAttribute("error", "Wrong login, authentication error");

        return "index";

    }

}

