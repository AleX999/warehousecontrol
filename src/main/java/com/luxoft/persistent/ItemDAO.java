package com.luxoft.persistent;

import com.luxoft.domain.Item;

import java.util.List;

public interface ItemDAO {
    Item findOne(long id);

    List<Item> findAll();

    void create(Item entity);

    Item update(Item entity);

    void delete(Item entity);

    void deleteById(long entityId);
}
