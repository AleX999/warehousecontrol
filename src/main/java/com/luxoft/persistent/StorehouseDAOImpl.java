package com.luxoft.persistent;

import com.luxoft.domain.Storehouse;
import org.springframework.stereotype.Repository;

@Repository
public class StorehouseDAOImpl extends AbstractJPADAO<Storehouse> implements StorehouseDAO {

    public StorehouseDAOImpl() {
        super();
        setClazz(Storehouse.class);
    }

}
