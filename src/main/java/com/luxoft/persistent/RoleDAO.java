package com.luxoft.persistent;

import com.luxoft.domain.Role;

import java.util.List;


public interface RoleDAO {
    Role findOne(long id);

    List<Role> findAll();

    void create(Role entity);

    Role update(Role entity);

    void delete(Role entity);

    void deleteById(long entityId);
}
