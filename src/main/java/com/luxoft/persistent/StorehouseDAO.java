package com.luxoft.persistent;

import com.luxoft.domain.Storehouse;

import java.util.List;

public interface StorehouseDAO {
    Storehouse findOne(long id);

    List<Storehouse> findAll();

    void create(Storehouse entity);

    Storehouse update(Storehouse entity);

    void delete(Storehouse entity);

    void deleteById(long entityId);

}
