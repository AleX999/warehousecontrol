package com.luxoft.persistent;

import com.luxoft.domain.Item;
import org.springframework.stereotype.Repository;

@Repository
public class ItemDAOImpl extends AbstractJPADAO<Item> implements ItemDAO {

    public ItemDAOImpl() {
        super();
        setClazz(Item.class);
    }

}
