package com.luxoft.persistent;

import com.luxoft.domain.User;

import java.util.List;

public interface UserDAO {
    User findOne(long id);

    List<User> findAll();

    void create(User entity);

    User update(User entity);

    void delete(User entity);

    void deleteById(long entityId);

    User findByEmail(String email);

}
