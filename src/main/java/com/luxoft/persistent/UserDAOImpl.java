package com.luxoft.persistent;

import com.luxoft.domain.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl extends AbstractJPADAO<User> implements UserDAO {

    public UserDAOImpl() {
        super();
        setClazz(User.class);
    }

    @Override
    public User findByEmail(String email) {
        User user = (User) entityManager.createQuery("SELECT u FROM User u WHERE u.email = '"
                + email + "'").getSingleResult();
        return user;
    }
}