package com.luxoft.persistent;

import com.luxoft.domain.Role;
import org.springframework.stereotype.Repository;

@Repository
public class RoleDAOImpl extends AbstractJPADAO<Role> implements RoleDAO {

    public RoleDAOImpl() {
        super();
        setClazz(Role.class);
    }

}
