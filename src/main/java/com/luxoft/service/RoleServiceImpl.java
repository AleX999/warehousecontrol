package com.luxoft.service;

import com.luxoft.domain.Role;
import com.luxoft.domain.User;
import com.luxoft.persistent.RoleDAO;
import com.luxoft.persistent.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    UserDAO userDAO;

    @Autowired
    RoleDAO roleDAO;

    @Override
    public Role setUserRoleToUser(User user) {
        Role role = new Role();
        role.setUser(user);
        roleDAO.create(role);

        user.setRole(role);

        userDAO.update(user);
        return role;
    }

    @Override
    public Role setAdminRoleToUser(User user) {
        Role role = new Role();
        role.setAuthority("ROLE_ADMIN");
        role.setUser(user);
        roleDAO.create(role);

        user.setRole(role);

        userDAO.update(user);
        return role;
    }

    @Override
    public Role getUserRole(User user) {

        return user.getRole();
    }
}
