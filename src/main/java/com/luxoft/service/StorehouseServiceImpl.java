package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.Storehouse;
import com.luxoft.persistent.ItemDAO;
import com.luxoft.persistent.StorehouseDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StorehouseServiceImpl implements StorehouseService {

    @Autowired
    StorehouseDAO storehouseDAO;

    @Autowired
    ItemDAO itemDAO;

    @Override
    public void addStorehouse(Storehouse storehouse) {
        storehouseDAO.create(storehouse);
    }

    @Override
    public Long getStorehouseCapacity(Storehouse storehouse) {
        return storehouse.getCapacity();
    }

    @Override
    public void deleteStorehouse(Storehouse storehouse) {
        storehouseDAO.delete(storehouse);
    }

    @Override
    public void deleteStorehouseById(Long id) {
        storehouseDAO.deleteById(id);
    }

    @Override
    public Storehouse updateStorehouse(Storehouse storehouse) {
        return storehouseDAO.update(storehouse);
    }

    @Override
    public Storehouse getStorehouseById(Long id) {
        return storehouseDAO.findOne(id);
    }

    @Override
    public List<Item> getAllItemsFromStorehouse(Storehouse storehouse) {
        return storehouse.getItem();
    }
}
