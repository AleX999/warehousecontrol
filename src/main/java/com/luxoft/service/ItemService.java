package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.Storehouse;
import com.luxoft.domain.User;
import com.luxoft.exceptions.CantTakeLessThanOneException;
import com.luxoft.exceptions.HaveNoMoreCapacityException;
import com.luxoft.exceptions.WrongStoreDateException;

import java.util.Date;
import java.util.List;

public interface ItemService {

    void addItem(Item item);

    void addItem(Item item, User user) throws HaveNoMoreCapacityException, CantTakeLessThanOneException;

    void deleteItem(Item item);

    void deleteItemById(Long id);

    Item updateItem(Item item);

    Item getItemById(Long id);

    Item changeToStoreDate(Item item, Date date) throws WrongStoreDateException;

    List<Item> getItemsInRangeBetweenDates(Date startDate, Date endDate);

    List<Item> getAllStoredItems();

    Item unstoreItem(Long id);

    User getItemOwnerByItem(Item item);

    Storehouse getStorehouseByItem(Item item);

    Item setItemCapacity(Item item, Long capacity);

    Item setComment(Item item, String comment);

    List<Item> getAllStoredItemsByUser(User user);


}
