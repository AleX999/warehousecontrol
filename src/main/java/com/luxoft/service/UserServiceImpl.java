package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.User;
import com.luxoft.persistent.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public void createUser(User user) {
        userDAO.create(user);
    }

    @Override
    public User getUserById(Long id) {
        return userDAO.findOne(id);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public List<User> getUsersList() {
        return userDAO.findAll();
    }

    @Override
    public List<String> getUsersEmails() {
        List<User> users = new ArrayList<User>();
        users = userDAO.findAll();
        List<String> emails = new ArrayList<>();
        for (User user : users) {
            emails.add(user.getEmail());
        }
        return emails;
    }

    @Override
    public User updateUser(User user) {
        return userDAO.update(user);
    }

    @Override
    public List<Item> getUserStoringItems(User user) {
        List<Item> items = user.getItems();
        List<Item> storingItems = new ArrayList<>();

        for (Item item : items) {
            if (item.getStoring() == true)
                storingItems.add(item);
        }

        return storingItems;

    }

    @Override
    public List<Item> getUserItems(User user) {
        return user.getItems();
    }

    @Override
    public User setAsEmployee(User user) {
        user.setEnabled(true);
        return user;
    }

    @Override
    public void deleteUser(User user) {
        userDAO.delete(user);
    }

    @Override
    public void deleteUserById(Long id) {
        userDAO.deleteById(id);

    }

}
