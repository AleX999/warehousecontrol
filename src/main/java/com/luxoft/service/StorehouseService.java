package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.Storehouse;

import java.util.List;

public interface StorehouseService {

    void addStorehouse(Storehouse storehouse);

    void deleteStorehouse(Storehouse storehouse);

    void deleteStorehouseById(Long id);

    Long getStorehouseCapacity(Storehouse storehouse);

    Storehouse updateStorehouse(Storehouse storehouse);

    Storehouse getStorehouseById(Long id);

    List<Item> getAllItemsFromStorehouse(Storehouse storehouse);

}
