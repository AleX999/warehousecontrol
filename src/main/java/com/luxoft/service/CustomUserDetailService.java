package com.luxoft.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserService userService;

    public UserDetails loadUserByUsername(String email) {
        com.luxoft.domain.User u = userService.getUserByEmail(email);
        if (u != null) {
            return new User(u.getEmail(), u.getPassword(), Collections.singleton(createAuthority(u)));
        } else {
            throw new UsernameNotFoundException("user not found");
        }
    }

    private GrantedAuthority createAuthority(com.luxoft.domain.User u) {
        return new SimpleGrantedAuthority(u.getRole().getAuthority());
    }

}