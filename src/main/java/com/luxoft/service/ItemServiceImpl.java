package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.Storehouse;
import com.luxoft.domain.User;
import com.luxoft.exceptions.CantTakeLessThanOneException;
import com.luxoft.exceptions.HaveNoMoreCapacityException;
import com.luxoft.exceptions.WrongStoreDateException;
import com.luxoft.persistent.ItemDAO;
import com.luxoft.persistent.StorehouseDAO;
import com.luxoft.persistent.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    private ItemDAO itemDAO;

    @Autowired
    private StorehouseDAO storehouseDAO;

    @Autowired
    private UserDAO userDAO;

    @Override
    public void addItem(Item item) {
        itemDAO.create(item);
    }

    @Override
    public void addItem(Item item, User user) throws HaveNoMoreCapacityException, CantTakeLessThanOneException {
        Storehouse storehouse = storehouseDAO.findOne(1L);
        Long capacity = storehouse.getCapacity();
        if (item.getCellsTaken() <= 0)
            throw new CantTakeLessThanOneException();
        if (item.getCellsTaken() > storehouse.getCapacity())
            throw new HaveNoMoreCapacityException();
        storehouse.setCapacity(capacity - item.getCellsTaken());
        item.setUser(user);
        item.setStorehouse(storehouse);

        itemDAO.update(item);
        storehouseDAO.update(storehouse);
    }

    @Override
    public void deleteItem(Item item) {
        itemDAO.delete(item);
    }

    @Override
    public void deleteItemById(Long id) {
        itemDAO.deleteById(id);
    }

    @Override
    public Item updateItem(Item item) {
        return itemDAO.update(item);
    }

    @Override
    public Item getItemById(Long id) {
        return itemDAO.findOne(id);
    }

    @Override
    public Item changeToStoreDate(Item item, Date date) throws WrongStoreDateException {
        if (item.getArrivalDate().before(date) && date.after(new Date())) {
            item.setStoreUntil(date);
        } else throw new WrongStoreDateException();
        //    itemDAO.update(item);
        return item;
    }

    @Override
    public List<Item> getItemsInRangeBetweenDates(Date startDate, Date endDate) {

        List<Item> items = new ArrayList<>();
        List<Item> itemsInRange = new ArrayList<>();
        items = itemDAO.findAll();

        for (Item item : items) {

            if (item.getArrivalDate().after(startDate) && item.getArrivalDate().before(endDate))
                itemsInRange.add(item);
        }

        return itemsInRange;
    }

    @Override
    public Item unstoreItem(Long id) {
        Item item = itemDAO.findOne(id);
        item.setStoring(false);
        Storehouse storehouse = item.getStorehouse();

        Long capacity = storehouse.getCapacity();
        storehouse.setCapacity(capacity + item.getCellsTaken());

        storehouseDAO.update(storehouse);
        itemDAO.update(item);
        return item;
    }

    @Override
    public List<Item> getAllStoredItems() {
        List<Item> items = new ArrayList<>();
        List<Item> storingItems = new ArrayList<>();

        items = itemDAO.findAll();

        for (Item item : items) {
            if (item.getStoring() == true)
                storingItems.add(item);
        }

        return storingItems;
    }


    @Override
    public User getItemOwnerByItem(Item item) {
        return item.getUser();
    }

    @Override
    public List<Item> getAllStoredItemsByUser(User user) {
        List<Item> items = itemDAO.findAll();
        List<Item> storingItems = new ArrayList<>();

        for (Item item : items) {
            if (item.getUser().getId() == user.getId() && item.getStoring() == true) {
                storingItems.add(item);
            }
        }


        return storingItems;
    }

    @Override
    public Item setComment(Item item, String comment) {
        item.setComment(comment);
        return item;
    }

    @Override
    public Item setItemCapacity(Item item, Long capacity) {
        item.setCellsTaken(capacity);
        return item;
    }

    @Override
    public Storehouse getStorehouseByItem(Item item) {
        return item.getStorehouse();
    }

}
