package com.luxoft.service;

import com.luxoft.domain.Role;
import com.luxoft.domain.User;
import org.springframework.stereotype.Service;

@Service
public interface RoleService {

    Role setUserRoleToUser(User user);

    Role setAdminRoleToUser(User user);

    Role getUserRole(User user);

}
