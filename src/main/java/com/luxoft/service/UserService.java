package com.luxoft.service;

import com.luxoft.domain.Item;
import com.luxoft.domain.User;

import java.util.List;

public interface UserService {

    void createUser(User user);

    User getUserById(Long id);

    User getUserByEmail(String email);

    List<User> getUsersList();

    List<String> getUsersEmails();

    User updateUser(User user);

    void deleteUser(User user);

    void deleteUserById(Long id);

    User setAsEmployee(User user);

    List<Item> getUserItems(User user);

    List<Item> getUserStoringItems(User user);

}
