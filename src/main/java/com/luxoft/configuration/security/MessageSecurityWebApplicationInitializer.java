package com.luxoft.configuration.security;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;


//automatically register the springSecurityFilterChain Filter for every URL in your application

public class MessageSecurityWebApplicationInitializer
        extends AbstractSecurityWebApplicationInitializer {

}
