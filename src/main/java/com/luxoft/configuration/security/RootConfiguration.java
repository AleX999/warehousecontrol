package com.luxoft.configuration.security;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


// loading all configuration within the same package (and child packages) as RootConfiguration

@Configuration
@ComponentScan
public class RootConfiguration {
}
