$(document).ready(function () {

    $(".validate").on("submit", function (e) {

        var password = $(this).find("#userPassword").val();
        if (password.length < 4) {
            e.preventDefault();
            $("#passwordError").html("Password very short");
        } else {
            $("#passwordError").html("");
        }

        var confirmPassword = $(this).find("#userConfirmPassword").val();
        if (confirmPassword != password) {
            e.preventDefault();
            $("#confirmPasswordError").html("password is different");
        } else {
            $("#confirmPasswordError").html("");
        }
    });

});



