$(document).ready(function () {

    $(".validate").on("submit", function (e) {
        var firstName = $(this).find("#userFirstName").val();
        if (firstName.length < 2) {
            e.preventDefault();
            $("#firstNameError").html("Wrong first name");
        } else {
            $("#firstNameError").html("");
        }

        var lastName = $(this).find("#userLastName").val();
        if (lastName.length < 2) {
            e.preventDefault();
            $("#lastNameError").html("Wrong last name");
        } else {
            $("#lastNameError").html("");
        }

        var email = $(this).find("#userEmail").val();
        var RE = /^[0-9a-zA-Z_\.]+@[0-9a-zA-Z_\.]+\.[a-z]{2,3}$/;
        if (!RE.test(email)) {
            e.preventDefault();
            $("#emailError").html("Wrong email<br/>");
        } else {
            $("#emailError").html("");
        }

        var password = $(this).find("#userPassword").val();
        if (password.length < 4) {
            e.preventDefault();
            $("#passwordError").html("Password very short");
        } else {
            $("#passwordError").html("");
        }

        var confirmPassword = $(this).find("#userConfirmPassword").val();
        if (confirmPassword != password) {
            e.preventDefault();
            $("#confirmPasswordError").html("password is different");
        } else {
            $("#confirmPasswordError").html("");
        }
    });


    $(".validateItemAdd").on("submit", function (e) {

        var capacity = $(this).find("#capacity").val();
        var RE = /^\d+$/;
        if (!RE.test(capacity)) {
            e.preventDefault();
            $("#capacityError").html("Wrong capacity");
        } else {
            $("#capacityError").html("");
        }
    });

    $(".validateItemDelete").on("submit", function (e) {

        var id = $(this).find("#idItem").val();
        var RE = /^\d+$/;
        if (!RE.test(id)) {
            e.preventDefault();
            $("#idItemError").html("Wrong id");
        } else {
            $("#idItemError").html("");
        }
    });

    $(function () {
        $("#listItems").tablesorter({ sortList: [
            [1, 0]
        ] });
    });
});



