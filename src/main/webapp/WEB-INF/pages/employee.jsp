<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/jQuery/jquery-ui.css"/>">
</head>
<body>
<div class="head">
    <h1 align="center">Store House - We know how to properly store</h1>
</div>
<br/>

<div class="container">

    <div class="col-xs-9">
        <div class="test">
        <form action="/search" method="post">
            <fieldset>
                <legend>Please Input Client's Email</legend>

                <div class="col-xs-10 form-group">
                    <input type="text" class="form-control" placeholder="Client Email" name="search"/>
                    <span class="error"> ${ClientNotFound} </span>
                </div>
                <button type="submit" class="col-xs-2 btn">Search</button>
            </fieldset>
        </form>
        </div>
        <br/>

        <div>
            <table class="table table-striped table-bordered table-condensed table-hover" id="listItems">
            <caption>List of Clients</caption>

                <thead>
                <tr>
                    <th>№</th>
                    <th>Email</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${listClients}" var="client">
                    <tr>
                        <td>${client.id}</td>
                        <td><a href="/search?email=${client.email}">${client.email}</a></td>
                        <td>${client.firstName}</td>
                        <td>${client.lastName}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <div class="col-xs-3">
        <div class="test">
            <form action="/logout" method="post">
                <fieldset>
                    <legend>Info:</legend>

                    <div class="form-group">
                        <p>First Name: ${user.firstName}</p>
                    </div>

                    <div class="form-group">
                        <p>Last Name: ${user.lastName}</p>
                    </div>

                    <div class="form-group">
                        <p>Email: ${user.email}</p>
                    </div>

                    <div class="form-group">
                        <a class="col-xs-3" href="/updateinfo">Personality</a>
                        <button type="submit" class="col-xs-5 col-xs-offset-4 btn btn-primary">Logout</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="navbar-fixed-bottom footer">
        <div align="center">© Created by Alexey, Andrew and Konstantin 2014</div>
    </div>
</div>
<script src="<c:url value="/resources/jQuery/jquery.js"/>"></script>
<script src="<c:url value="/resources/jQuery/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/calendar.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.tablesorter.js"/>"></script>
<script src="<c:url value="/resources/js/validation.js"/>"></script>
</body>
</html>