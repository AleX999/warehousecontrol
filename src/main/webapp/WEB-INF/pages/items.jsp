<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Employee</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/jQuery/jquery-ui.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">

</head>
<body>
<div class="head">
    <h1 align="center">Store House - We know how to properly store</h1>
</div>
<br/>

<div class="container">

    <div class="col-xs-9">
        <div class="test">
            <form action="/employee" method="post">
                <fieldset>
                    <legend>Client's Info</legend>
                    <div class="col-xs-10">
                        <p>Client's Name: ${client.firstName} ${client.lastName}

                        <p>Email: ${client.email}</p>
                    </div>
                    <button type="submit" class="col-xs-2 btn">Go back</button>
                </fieldset>
            </form>
        </div>
        <br/>

        <div class="test">
            <div class="row">
                <form action="/add" class="col-xs-6 form-group validateItemAdd" method="post">
                    <fieldset>
                        <legend>Add Unit</legend>
                        <div class="col-xs-9 form-group">
                            <input type="text" class="form-control" placeholder="Cells taken" name="cellsTaken"
                                   id="capacity"/>
                            <span class="error">${capacityError}</span>
                            <span class="error" id="capacityError"></span><br/>
                            <input id="dateEntry" type="text" class="form-control" placeholder="Store until"
                                   name="date"/>
                            <span class="error">${dateError}</span><br/>
                            <textarea style="resize: vertical;" class="form-control" placeholder="Comments"
                                      name="comment"/></textarea>
                        </div>
                        <button type="submit" class="col-xs-3 btn">Add</button>
                        <span class="success">${itemAdd}</span>
                        <%--<p>${clientNotSelected}</p>--%>

                    </fieldset>
                </form>

                <form action="/delete" class="col-xs-6 form-group validateItemDelete" method="post">
                    <fieldset>
                        <legend>Release Unit</legend>
                        <div class="col-xs-9 form-group">
                            <input type="text" class="form-control" placeholder="Article" name="release" id="idItem"/>
                            <span class="error">${searchItem}</span>
                            <span class="error" id="idItemError"></span>
                            <span class="success">${itemDelete}</span>
                        </div>
                        <button type="submit" class="col-xs-3 btn">Release</button>
                    </fieldset>
                </form>
            </div>
        </div>
        <br/>

        <div>
            <table class="table table-striped table-bordered table-condensed table-hover" id="listItems">
                <caption>Product Store List</caption>

                <thead>
                <tr>
                    <th>Article</th>
                    <th>Incoming Date</th>
                    <th>Store Until</th>
                    <th>Cells Taken</th>
                    <th>Comments</th>
                </tr>
                </thead>

                <tbody>
                <c:forEach items="${listItems}" var="item">
                    <tr>
                        <td>${item.id}</td>
                        <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                            value="${item.arrivalDate}"/></td>
                        <td><fmt:formatDate type="both" dateStyle="short" timeStyle="short"
                                            value="${item.storeUntil}"/></td>
                        <td>${item.cellsTaken}</td>
                        <td>${item.comment}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>

    </div>
    <div class="col-xs-3">
        <div class="test">
            <form action="/logout" method="post">
                <fieldset>
                    <legend>Info:</legend>

                    <div class="form-group">
                        <p>First Name: ${user.firstName}</p>
                    </div>

                    <div class="form-group">
                        <p>Last Name: ${user.lastName}</p>
                    </div>

                    <div class="form-group">
                        <p>Email: ${user.email}</p>
                    </div>

                    <div class="form-group">
                        <a class="col-xs-3" href="/updateinfo">Personality</a>
                        <button type="submit" class="col-xs-5 col-xs-offset-4 btn btn-primary">Logout</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
    <div class="navbar-fixed-bottom footer">
        <div align="center">© Created by Alexey, Andrew and Konstantin 2014</div>
    </div>
</div>
<script src="<c:url value="/resources/jQuery/jquery.js"/>"></script>
<script src="<c:url value="/resources/jQuery/jquery-ui.js"/>"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/calendar.js"/>"></script>
<script src="<c:url value="/resources/js/jquery.tablesorter.js"/>"></script>
<script src="<c:url value="/resources/js/validation.js"/>"></script>
</body>
</html>