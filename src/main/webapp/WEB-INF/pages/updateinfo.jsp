<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Update Info</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
</head>
<body>
<div class="head">
    <h1 align="center">Store House - We know how to properly store</h1>
</div>
<br/>

<div class="container">

    <div class="col-xs-4">
        <form action="/updatepasswd" method="post" class="test validate">
            <fieldset>
                <legend>Change Password Form</legend>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" id="userPassword"
                           name="password">
                    <span class="error" id="passwordError"></span>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Confirm Password" id="userConfirmPassword"
                           name="confirmPassword">
                    <span class="error" id="confirmPasswordError"></span>
                </div>

                <div class="form-group">
                    <a class="col-xs-4" href="/goback">Go Back</a>
                    <button type="submit" class="col-xs-4 col-xs-offset-4 btn btn-primary">Update</button>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="navbar-fixed-bottom footer">
        <div align="center">© Created by Alexey, Andrew and Konstantin 2014</div>
    </div>
</div>
<script src="<c:url value="/resources/jQuery/jquery.js"/>"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/validchangepass.js"/>"></script>
</body>
</html>