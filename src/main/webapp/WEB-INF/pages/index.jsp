<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Store House</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
    <script type="text/javascript" src="<c:url value="http://code.jquery.com/jquery.min.js"/>"></script>
    <script type="text/javascript" src="<c:url value="/resources/bootstrap/js/bootstrap.min.js"/>"></script>
</head>
<body>
<div class="head">
    <h1 align="center">Store House - We know how to properly store</h1>
</div>
<div class="container">

    <br/>

    <div class="row">
        <div class="col-xs-8">
            <div class="test">
                <%-----------------------------------------------------------%>
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="<c:url value="/resources/img/01.png"/>">
                        </div>

                        <div class="item">
                            <img src="<c:url value="/resources/img/02.jpg"/>">
                        </div>

                        <div class="item">
                            <img src="<c:url value="/resources/img/03.jpg"/>">
                        </div>

                        <div class="item">
                            <img src="<c:url value="/resources/img/04.jpg"/>">
                        </div>
                    </div>
                </div>
                <%-----------------------------------------------------------%>
            </div>
        </div>

        <div class="col-xs-4">
            <div class="test">
                <form action="<c:url value='j_spring_security_check' />" method="post">
                    <fieldset>
                        <legend>Login Please:</legend>

                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Email" id="Email" name="j_username">
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Password" id="Password"
                                   name="j_password">
                        </div>

                        <div class="form-group">
                            <a class="col-xs-4" href="/register">Registration</a>
                            <button type="submit" class="col-xs-4 col-xs-offset-4 btn btn-primary">Login</button>
                        </div>
                        <%--<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>--%>
                    </fieldset>
                </form>
                <span class="error">${error}</span>
            </div>
        </div>
    </div>
    <div class="navbar-fixed-bottom footer">
        <div align="center">© Created by Alexey, Andrew and Konstantin 2014</div>
    </div>
</div>
<script src="<c:url value="/resources/jQuery/jquery.js"/>"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
</body>
</html>