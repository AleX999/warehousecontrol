<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/bootstrap/css/bootstrap.css"/>">
    <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/style.css"/>">
</head>
<body>
<div class="head">
    <h1 align="center">Store House - We know how to properly store</h1>
</div>
<br/>

<div class="container">

    <div class="col-xs-4">
        <form action="/register" method="post" class="test validate">
            <fieldset>
                <legend>Please Fill The Form</legend>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="First Name" id="userFirstName"
                           name="firstName">
                    <span class="error" id="firstNameError"></span>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Last Name" id="userLastName"
                           name="lastName">
                    <span class="error" id="lastNameError"></span>
                </div>

                <div class="form-group">
                    <input type="text" class="form-control" placeholder="Email" id="userEmail" name="email">
                    <span class="error" id="emailError"></span>
                    <span class="error"> ${email}</span>
                </div>
                <br/>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Password" id="userPassword"
                           name="password">
                    <span class="error" id="passwordError"></span>
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" placeholder="Confirm Password" id="userConfirmPassword"
                           name="confirmPassword">
                    <span class="error" id="confirmPasswordError"></span>
                </div>

                <div class="form-group">
                    <a class="col-xs-4" href="/">Cancel</a>
                    <button type="submit" class="col-xs-4 col-xs-offset-4 btn btn-primary">Submit</button>
                </div>
                <%--<span class="error" id="firstNameError"></span>--%>
                <%--<span class="error" id="lastNameError"></span>--%>
                <%--<span class="error" id="emailError"></span>--%>
                <%--<span class="error" id="passwordError"></span>--%>
                <%--<span class="error" id="confirmPasswordError"></span>--%>
            </fieldset>
        </form>
    </div>
    <div class="navbar-fixed-bottom footer">
        <div align="center">© Created by Alexey, Andrew and Konstantin 2014</div>
    </div>
</div>
<script src="<c:url value="/resources/jQuery/jquery.js"/>"></script>
<script src="<c:url value="/resources/bootstrap/js/bootstrap.js"/>"></script>
<script src="<c:url value="/resources/js/validation.js"/>"></script>
</body>
</html>